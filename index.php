<?php 
// Template Name: Página Inicial
?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tráfego Pago</title>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
</head>
<body>

  <div class="container">

    <header>

      <section class="header">

        <div class="logo-div">
          <h1 class="logo-content">LOGO</h1>
        </div>
    
        <h1 class="slogan">
          <?php the_field('sessao_slogan_slogan'); ?>
        </h1>
    
        <h4 class="presentation">
          <?php the_field('sessao_slogan_slogan_subtitulo'); ?>
        </h4>
        
        <button class="btn-appointment">
          Agendar Consulta
        </button>

      </section>      
  
    </header>
  
    <main>

      <section class="about">

        <h1 class="about-title">Sobre a Empresa</h1>
      
        <div class="about-content">
  
          <img class="about_image" src=<?php the_field('sobre_a_empresa_imagem_sobre_a_empresa_1'); ?> alt="Foto do Local da Empresa" width="300px" height="300px"/>

          <div>
            <h2 class="about_main_content"><?php the_field('sobre_a_empresa_titulo_sobre_a_empresa_1'); ?></h2>
            <p class="about_text_content">
              <?php the_field('sobre_a_empresa_descricao_sobre_a_empresa_1'); ?>
            </p>
          </div>

  
        </div>

        <div class="about-content2">

          <div class="about-div2">
            <img src=<?php the_field('sobre_a_empresa_imagem_sobre_a_empresa_esquerda_superior');?> alt="sobre1" width="100px" height="100px">
            <p class="about-text-content2">
              <?php the_field('sobre_a_empresa_texto_sobre_a_empresa_esquerda_superior'); ?>
            </p>
          </div>

          <div class="about-div2">
            <img src=<?php the_field('sobre_a_empresa_imagem_sobre_a_empresa_direita_superior'); ?> alt="sobre2" width="100px" height="100px">
            <p class="about-text-content2">
              <?php the_field('sobre_a_empresa_texto_sobre_a_empresa_direita_superior'); ?>
            </p>
          </div>

          <div class="about-div2">
            <img src=<?php the_field('sobre_a_empresa_imagem_sobre_a_empresa_esquerda_inferior'); ?> alt="sobre3" width="100px" height="100px">
            <p class="about-text-content2">
              <?php the_field('sobre_a_empresa_texto_sobre_a_empresa_esquerda_inferior'); ?>
            </p>
          </div>

          <div class="about-div2">
            <img src=<?php the_field('sobre_a_empresa_imagem_sobre_a_empresa_direita_inferior'); ?> alt="sobre4" width="100px" height="100px">
            <p class="about-text-content2">
              <?php the_field('sobre_a_empresa_texto_sobre_a_empresa_direita_inferior'); ?>
            </p>
          </div>

        </div>

      </section>
      
      
      <section class="cases">

        <h1 class="cases-title">Cases de Sucesso</h1>
  
        <div class="slide-container swiper">
    
          <div class="slide-content">
    
            <div class="card-wrapper swiper-wrapper">
    
              <!-- Card 1   -->
              <div class="card swiper-slide">
    
                <div class="image-content">
    
                  <span class="overlay"></span>
    
                  <div class="card-image">
                    <img src=<?php the_field('cases_de_sucesso_case_1_imagem'); ?> alt="case1" class="card-img" />
                  </div>
    
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_1_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_1_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 2   -->
              <div class="card swiper-slide">
    
                <div class="image-content">
    
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_2_imagem'); ?> alt="case2" class="card-img" />
                  </div>
    
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_2_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_2_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
              
              <!-- Card 3   -->
              <div class="card swiper-slide">
    
                <div class="image-content">
    
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_3_imagem'); ?> alt="case3" class="card-img" />
                  </div>
    
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_3_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_3_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 4   -->
              <div class="card swiper-slide">
    
                <div class="image-content">
    
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_4_imagem'); ?> alt="case4" class="card-img" />
                  </div>
    
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_4_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_4_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 5   -->
              <div class="card swiper-slide">
    
                <div class="image-content">
    
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_5_imagem'); ?> alt="case5" class="card-img" />
                  </div>
    
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_5_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_5_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 6   -->
              <div class="card swiper-slide">
                <div class="image-content">
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_6_imagem'); ?> alt="case6" class="card-img" />
                  </div>
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_6_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_6_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 7   -->
              <div class="card swiper-slide">
                <div class="image-content">
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_7_imagem'); ?> alt="case7" class="card-img" />
                  </div>
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_7_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_7_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
              <!-- Card 8   -->
              <div class="card swiper-slide">
                <div class="image-content">
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_8_imagem'); ?> alt="case8" class="card-img" />
                  </div>
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_8_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_8_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
              
              <!-- Card 9   -->
              <div class="card swiper-slide">
                <div class="image-content">
                  <span class="overlay"></span>
    
                  <div class="card-image">
                  <img src=<?php the_field('cases_de_sucesso_case_9_imagem'); ?> alt="case9" class="card-img" />
                  </div>
                </div>
    
                <div class="card-content">
    
                  <h2 class="name"><?php the_field('cases_de_sucesso_case_9_nome'); ?></h2>
                  <p class="description">
                  <?php the_field('cases_de_sucesso_case_9_comentario'); ?>
                  </p>
    
                  <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" width="50px" height="50px"> 
    
                </div>
    
              </div>
    
            </div>
    
          </div>
    
          <div class="swiper-button-next swiper-navBtn"></div>
          <div class="swiper-button-prev swiper-navBtn"></div>
          <div class="swiper-pagination"></div>
    
        </div>

        
      </section>
      
      <section class="depositions">

        <h1 class="deposition-title">Depoimentos</h1>
  
        <div class="slide-container swiper">
    
          <div class="slide-content">
    
            <div class="card-wrapper swiper-wrapper">
    
              <!-- Card 1   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_1_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_1_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_1_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 2   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_2_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_2_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_2_nome'); ?></h2>
    
                </div>
    
              </div>
              
              <!-- Card 3   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_3_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_3_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_3_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 4   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_4_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_4_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_4_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 5   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_5_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_5_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_5_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 6   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_6_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_6_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_6_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 7   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_7_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_7_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_7_nome'); ?></h2>
    
                </div>
    
              </div>
    
              <!-- Card 8   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_8_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_8_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_8_nome'); ?></h2>
    
                </div>
    
              </div>
              
              <!-- Card 9   -->
              <div class="card-deposition swiper-slide">

                <div class="rating-img">
                  <img src=<?php the_field('depoimentos_case_9_imagem'); ?> alt="" width="200px">
                </div> 

                <div class="card-content-deposition">
    
                  <p class="description-deposition">
                  <?php the_field('depoimentos_case_9_comentario'); ?>
                  </p>
                  <h2 class="name-deposition"><?php the_field('depoimentos_case_9_nome'); ?></h2>
    
                </div>
    
              </div>
    
            </div>
    
          </div>
    
          <div class="swiper-button-next swiper-navBtn"></div>
          <div class="swiper-button-prev swiper-navBtn"></div>
          <div class="swiper-pagination"></div>
    
        </div>

      </section>

      <section class="contact">
        
        <h1 class="contact-title">Contate - nos</h1>
        <div class="contact-div">
          <div class="whatsapp">
            <img src="<?php echo get_stylesheet_directory_uri() ?>./imagens/whatsapp.png" alt="" width="150px" height="150px">
          </div>

          <div class="opening-hours">
            <h2 class="opening-subtitle">Horário de Funcionamento :</h2>
            <h3 class="opening-time">Segunda: <?php the_field('contato_segunda-feira'); ?></h3>
            <h3 class="opening-time">Terça: <?php the_field('contato_terca-feira'); ?></h3>
            <h3 class="opening-time">Quarta: <?php the_field('contato_quarta-feira'); ?></h3>
            <h3 class="opening-time">Quinta: <?php the_field('contato_quinta-feira'); ?></h3>
            <h3 class="opening-time">Sexta: <?php the_field('contato_sexta-feira'); ?></h3>
            <h3 class="opening-time">Sábado: <?php the_field('contato_sabado'); ?></h3>
            <h3 class="opening-time">Domingo: <?php the_field('contato_domingo'); ?></h3>
          </div>
        </div>
          
      </section>      
  
    </main>
  
    <footer>
      
      <section class="footer">
        <hr class="line-detail">
        <p class="copy-text">@Copyright | CNPJ: 12. 840. 829/0001-88</p>
      </section>

    </footer>

  </div>  

</body>

<script src="<?php echo get_stylesheet_directory_uri() ?>./js/swiper-bundle.js"></script>
<script src="<?php echo get_stylesheet_directory_uri() ?>./js/script.js"></script>

</html>